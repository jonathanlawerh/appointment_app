package prepaid.jonathan.com.passdata;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    public ArrayList<Appointment> appointmentArrayList = new ArrayList<Appointment>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createSomeTestAppointmentToStartWith();
    }

    private void createSomeTestAppointmentToStartWith() {
        appointmentArrayList.add(new Appointment("Doctors Visit","Health", "Oct", 9, 2016, 9, 00, "AM"));
        appointmentArrayList.add(new Appointment("Hair Cut appointment","Personal","Oct", 10, 2016,9,30,"AM"));
        appointmentArrayList.add(new Appointment("Meeting with Accountant","Personal","Oct", 11, 2016,11,00,"AM"));
        appointmentArrayList.add(new Appointment("Boss/HR Meeting","Work","Oct", 12, 2016,2,30,"PM"));
        appointmentArrayList.add(new Appointment("Teacher Conference","School","Nov", 1, 2016,9,30,"AM"));
        appointmentArrayList.add(new Appointment("Dentist For Son","Health","Nov", 1, 2016,9,30,"AM"));
        appointmentArrayList.add(new Appointment("Dinner With Friends","Other","Nov", 1, 2016,9,30,"AM"));

        for (int i = 0; i < appointmentArrayList.size(); i++){
            populateTable(i);
        }
    }

    private void populateTable(int arrayListCounter) {
        TableLayout  appointmentTBL = (TableLayout) findViewById(R.id.tblTaskContent);

        TableRow newTableRow = new TableRow(this);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        newTableRow.setLayoutParams(lp);

        TextView txtTvName = new TextView(this);
        txtTvName.setLayoutParams(lp);
        txtTvName.setGravity(Gravity.CENTER);
        txtTvName.setText(appointmentArrayList.get(arrayListCounter).name);
        txtTvName.setWidth(140);
        txtTvName.setTextSize(12);
        txtTvName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        TextView txtTvType = new TextView(this);
        txtTvType.setLayoutParams(lp);
        txtTvType.setGravity(Gravity.CENTER);
        txtTvType.setText(appointmentArrayList.get(arrayListCounter).type);
        txtTvType.setWidth(93);
        txtTvType.setTextSize(12);
        txtTvType.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        TextView txtTvDate = new TextView(this);
        txtTvDate.setLayoutParams(lp);
        txtTvDate.setGravity(Gravity.CENTER);
        txtTvDate.setText(setToDateAndTime(appointmentArrayList.get(arrayListCounter)));
        txtTvDate.setWidth(97);
        txtTvDate.setTextSize(12);
        txtTvDate.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        newTableRow.addView(txtTvName);
        newTableRow.addView(txtTvType);
        newTableRow.addView(txtTvDate);
        appointmentTBL.addView(newTableRow, arrayListCounter+1);
    }

    private String setToDateAndTime(Appointment appointment) {
        //current Date
        long currentDateAndTime = System.currentTimeMillis();

        //date format
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy");

        //today's date formatted
        String todaysDate = dateFormat.format(currentDateAndTime);

        //task date formatted the same way
        String passDate = appointment.monthDate + " " + appointment.dayDate + ", " + appointment.yearDate;

        //compare today's date and pass date, if it matches return time
        if(Objects.equals(todaysDate, passDate)){ //Compare today's date and passed date, return time if dates match
            return appointment.hourTime +":" +appointment.minuteTime +" " +appointment.AMorPMTime;
        }
        return appointment.monthDate + " " + appointment.dayDate + ", " + appointment.yearDate;
    }

    public void AddAppointmentBtn(View view) {
        startActivityForResult(new Intent(this, AddAppointment.class), 1);
    }

    @Override
    //Returns information passed from addAppointmentactivity
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {

                //Creates a new appointment with the information passed
                appointmentArrayList.add(new Appointment(
                        data.getStringExtra("name"),data.getStringExtra("type"),
                        data.getStringExtra("monthOfYear"), data.getIntExtra("dayOfMonth", 0), data.getIntExtra("year", 1111),
                        data.getIntExtra("hour", 11),data.getIntExtra("minute", 11),data.getStringExtra("AMorPM")));
                //Displays new appointment on in the table
                populateTable(appointmentArrayList.size()-1);
            }
        }
    }

}
